package co.com.jose.pilae.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import co.com.jose.pilae.exception.genericexception.GenericException;
import co.com.jose.pilae.exception.preconditionexception.PreconditionException;

@ControllerAdvice
public class GlobalHandler {

	@ExceptionHandler({ Exception.class })
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public void genericException(Exception e) {
		//exceptions 
	}

	@ExceptionHandler({ GenericException.class })
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public @ResponseBody Map<String, String> genericException(GenericException e) {
		Map<String, String> mensaje = new HashMap<>();
		mensaje.put("mensaje", e.getMensaje());
		return mensaje;
	}

	@ExceptionHandler({ PreconditionException.class })
	@ResponseStatus(HttpStatus.PRECONDITION_REQUIRED)
	public @ResponseBody Map<String, String> preconditionException(PreconditionException e) {
		Map<String, String> mensaje = new HashMap<>();
		mensaje.put("mensaje", e.getMensaje());
		return mensaje;
	}

}
