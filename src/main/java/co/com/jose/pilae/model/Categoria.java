package co.com.jose.pilae.model;

import java.io.Serializable;

public class Categoria implements Serializable {

	private int id_categoria;

	private String categoria;

	private String descripcion;

	public co.com.jose.pilae.entity.Categoria asCategoriaEntidad() {
		co.com.jose.pilae.entity.Categoria categoria = null;
		categoria.setId_categoria(this.id_categoria);
		categoria.setCategoria(this.categoria);
		categoria.setDescripcion(this.descripcion);
		return categoria;
	}

	public int getId_categoria() {
		return id_categoria;
	}

	public void setId_categoria(int id_categoria) {
		this.id_categoria = id_categoria;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
