package co.com.jose.pilae.model;

import java.io.Serializable;
import java.util.Calendar;

public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id_usuario;

	private String identificacion;

	private String nombre;

	private String apellido;

	private Calendar fecha_nacimiento;

	private String numero_telefono;

	private String numero_celular;

	private String correo;

	private String contrasenia;

	private String imagen;

	private String residencia;

	private Genero genero;

	private Rol rol;

	public co.com.jose.pilae.entity.Usuario asUsuarioEntidad() {
		co.com.jose.pilae.entity.Usuario usuario = null;
		usuario.setId_usuario(this.id_usuario);
		usuario.setIdentificacion(this.identificacion);
		usuario.setNombre(this.nombre);
		usuario.setApellido(this.apellido);
		usuario.setFecha_nacimiento(this.fecha_nacimiento);
		usuario.setNumero_telefono(this.numero_telefono);
		usuario.setNumero_celular(this.numero_celular);
		usuario.setCorreo(this.correo);
		usuario.setContrasenia(this.contrasenia);
		usuario.setImagen(this.imagen);
		usuario.setResidencia(this.residencia);
		usuario.setRol(this.rol.asRolEntidad());
		usuario.setGenero(this.genero.asGeneroEntidad());
		return usuario;
	}

	public int getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Calendar getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(Calendar fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getNumero_telefono() {
		return numero_telefono;
	}

	public void setNumero_telefono(String numero_telefono) {
		this.numero_telefono = numero_telefono;
	}

	public String getNumero_celular() {
		return numero_celular;
	}

	public void setNumero_celular(String numero_celular) {
		this.numero_celular = numero_celular;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public String getResidencia() {
		return residencia;
	}

	public void setResidencia(String residencia) {
		this.residencia = residencia;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
