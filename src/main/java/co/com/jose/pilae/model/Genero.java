package co.com.jose.pilae.model;

import java.io.Serializable;

public class Genero implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id_genero;

	private String genero;

	private String descripcion;

	public co.com.jose.pilae.entity.Genero asGeneroEntidad() {
		co.com.jose.pilae.entity.Genero genero = null;
		genero.setId_genero(this.id_genero);
		genero.setGenero(this.genero);
		genero.setDescripcion(this.descripcion);

		return genero;
	}

	public int getId_genero() {
		return id_genero;
	}

	public void setId_genero(int id_genero) {
		this.id_genero = id_genero;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
