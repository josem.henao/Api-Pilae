package co.com.jose.pilae.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.jose.pilae.dominio.usuario.IUsuarioDominio;
import co.com.jose.pilae.model.Usuario;

@RestController
@RequestMapping(path = "/usuarios")
public class UsuarioControlador {

	@Autowired
	IUsuarioDominio usuarioDominio;

	@RequestMapping(value = "/insertar", method = RequestMethod.POST)
	public boolean insertarUsuario(@RequestBody Usuario usuario) {
		return usuarioDominio.insertarUsuario(usuario);
	}

	@RequestMapping(value = "/consultar", method = RequestMethod.GET)
	public Usuario consultarUsuario(@RequestBody String identificacion) {
		return usuarioDominio.consultarUsuarioPorIdentificacion(identificacion);
	}

	@RequestMapping(value = "/actualizar", method = RequestMethod.POST)
	public boolean actualizarUsuario(@RequestBody Usuario usuario) {
		return usuarioDominio.actualizarUsuario(usuario);
	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.POST)
	public boolean eliminarUsuario(@RequestBody Usuario usuario) {
		return usuarioDominio.eliminarUsuario(usuario);
	}

}
