package co.com.jose.pilae.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import co.com.jose.pilae.dominio.usuario.ITorneoDominio;
import co.com.jose.pilae.model.Torneo;

@RestController
@RequestMapping(path = "/torneos")
public class TorneoControlador {
	
	@Autowired
	ITorneoDominio torneoDominio;

	@RequestMapping(value = "/insertar", method = RequestMethod.POST)
	public boolean insertarTorneo(@RequestBody Torneo torneo) {
		return torneoDominio.insertarTorneo(torneo);
	}

	@RequestMapping(value = "/consultar", method = RequestMethod.GET)
	public Torneo consultarTorneo(@RequestBody int id_torneo) {
		return torneoDominio.consultarTorneoPorId(id_torneo);
	}

	@RequestMapping(value = "/actualizar", method = RequestMethod.POST)
	public boolean actualizarUsuario(@RequestBody Torneo torneo) {
		return torneoDominio.actualizarTorneo(torneo);
	}

	@RequestMapping(value = "/eliminar", method = RequestMethod.POST)
	public boolean eliminarUsuario(@RequestBody Torneo torneo) {
		return torneoDominio.eliminarTorneo(torneo);
	}
}
