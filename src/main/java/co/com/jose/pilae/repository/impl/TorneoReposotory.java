//package co.com.jose.pilae.repository.impl;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//
//import co.com.jose.pilae.entity.Torneo;
//import co.com.jose.pilae.repository.ITorneoRepository;
//import co.com.jose.pilae.repositoryJpa.ITorneoRepositoryJpa;
//
//public class TorneoReposotory implements ITorneoRepository {
//
//	@Autowired
//	ITorneoRepositoryJpa torneoRepositoryJpa;
//
//	@Override
//	public boolean insertarTorneo(Torneo torneo) {
//		Torneo tInsertado = torneoRepositoryJpa.save(torneo);
//		return tInsertado != null ? true : false;
//	}
//
//	@Override
//	public Torneo consultarTorneoPorId(int id_torneo) {
//		return null;
//	}
//
//	@Override
//	public List<Torneo> consultarTorneoPorNombre(String nombre) {
//		List<Torneo> torneos = null;
//		torneos = torneoRepositoryJpa.consultarPorNombre(nombre);
//		return torneos;
//	}
//
//	@Override
//	public boolean eliminarTorneo(Torneo torneo) {
//		boolean tEliminado = false;
//		try {
//			torneoRepositoryJpa.delete(torneo);
//			tEliminado = true;
//		} catch (Exception e) {
//			tEliminado = false;
//		}
//		return tEliminado;
//	}
//
//	@Override
//	public boolean actualizarTorneo(Torneo torneo) {
//		boolean tActualizado = false;
//		Torneo torneoBD = consultarTorneoPorId(torneo.getId_torneo());
//		if (torneoBD != null) {
//			torneoBD = modificarTorneo(torneoBD, torneo);
//			torneoRepositoryJpa.flush();
//			tActualizado = true;
//		}
//		return tActualizado;
//	}
//
//	private Torneo modificarTorneo(Torneo torneoBD, Torneo torneoNuevo) {
//		torneoBD.setId_torneo(torneoNuevo.getId_torneo());
//		torneoBD.setNombre(torneoNuevo.getNombre());
//		torneoBD.setDescripcion(torneoNuevo.getDescripcion());
//		torneoBD.setFecha_inicio(torneoNuevo.getFecha_inicio());
//		torneoBD.setFecha_fin(torneoNuevo.getFecha_fin());
//		torneoBD.setImagen(torneoNuevo.getImagen());
//		torneoBD.setPremiacion(torneoNuevo.getPremiacion());
//		torneoBD.setDesc_premiacion(torneoNuevo.getDesc_premiacion());
//		torneoBD.setGenero(torneoNuevo.getGenero());
//		torneoBD.setModalidad(torneoNuevo.getModalidad());
//		
//		return torneoBD;
//	}
//
//}
