// package co.com.jose.pilae.repository.impl;
//
// import org.springframework.beans.factory.annotation.Autowired;
//
// import co.com.jose.pilae.entity.Usuario;
// import co.com.jose.pilae.repository.IUsuarioRepository;
// import co.com.jose.pilae.repositoryJpa.IUsuarioRepositoryJpa;
//
// public class UsuarioRepository implements IUsuarioRepository {
//
// @Autowired
// IUsuarioRepositoryJpa usuarioRepositoryJpa;
//
// @Override
// public boolean insertarUsuario(Usuario usuario) {
// Usuario uInsertado = usuarioRepositoryJpa.save(usuario);
// return uInsertado != null ? true : false;
// }
//
// @Override
// public Usuario consultarUsuario(String identificacion) {
// Usuario uConsultado;
// uConsultado =
// usuarioRepositoryJpa.consultarPorIdentificacion(identificacion);
// return uConsultado;
// }
//
// @Override
// public boolean eliminarUsuario(Usuario usuario) {
// boolean uEliminado = false;
// try {
// usuarioRepositoryJpa.delete(usuario);
// uEliminado = true;
// } catch (Exception e) {
// uEliminado = false;
// }
// return uEliminado;
// }
//
// @Override
// public boolean actualizarUsuario(Usuario usuario) {
// boolean uActualizado = false;
// Usuario usuarioBD = consultarUsuario(usuario.getIdentificacion());
// if (usuarioBD != null) {
// usuarioBD = modificarUsuario(usuarioBD, usuario);
// usuarioRepositoryJpa.flush();
// uActualizado = true;
// }
// return uActualizado;
// }
//
// private Usuario modificarUsuario(Usuario usuarioBD, Usuario usuarioNuevo) {
// usuarioBD.setId_usuario(usuarioNuevo.getId_usuario());
// usuarioBD.setIdentificacion(usuarioNuevo.getIdentificacion());
// usuarioBD.setNombre(usuarioNuevo.getNombre());
// usuarioBD.setApellido(usuarioNuevo.getApellido());
// usuarioBD.setFecha_nacimiento(usuarioNuevo.getFecha_nacimiento());
// usuarioBD.setNumero_telefono(usuarioNuevo.getNumero_telefono());
// usuarioBD.setNumero_celular(usuarioNuevo.getNumero_celular());
// usuarioBD.setCorreo(usuarioNuevo.getCorreo());
// usuarioBD.setContrasenia(usuarioNuevo.getContrasenia());
// usuarioBD.setImagen(usuarioNuevo.getImagen());
// usuarioBD.setResidencia(usuarioNuevo.getResidencia());
// usuarioBD.setRol(usuarioNuevo.getRol());
// usuarioBD.setGenero(usuarioNuevo.getGenero());
// return usuarioBD;
// }
//
// }
