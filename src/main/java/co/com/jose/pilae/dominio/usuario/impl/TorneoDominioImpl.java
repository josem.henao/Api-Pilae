package co.com.jose.pilae.dominio.usuario.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.jose.pilae.dominio.usuario.ITorneoDominio;
import co.com.jose.pilae.model.Torneo;
import co.com.jose.pilae.repository.ITorneoRepository;

@Service
public class TorneoDominioImpl implements ITorneoDominio {

	@Autowired
	ITorneoRepository torneoRepositorio;

	@Override
	public boolean insertarTorneo(Torneo torneo) {
		torneoRepositorio.insertarTorneo(torneo.asTorneoEntidad());
		return false;
	}

	@Override
	public Torneo consultarTorneoPorId(int id_torneo) {
		torneoRepositorio.consultarTorneoPorId(id_torneo);
		return null;
	}

	@Override
	public List<Torneo> consultarTorneoPorNombre(String nombre) {
		List<co.com.jose.pilae.entity.Torneo> torneos = torneoRepositorio.consultarTorneoPorNombre(nombre);
		return torneosAsListModel(torneos);
	}

	private List<Torneo> torneosAsListModel(List<co.com.jose.pilae.entity.Torneo> torneos) {
		List<Torneo> torneosModel = null;
		for (co.com.jose.pilae.entity.Torneo t : torneos) {
			torneosModel.add(t.asTorneoModelo());
		}
		return torneosModel;
	}

	@Override
	public boolean actualizarTorneo(Torneo torneo) {
		boolean tActualizado = false;
		Torneo torneoBD = consultarTorneoPorId(torneo.getId_torneo());
		if (torneoBD != null) {
			torneoBD = modificarTorneo(torneoBD, torneo);
			torneoRepository.flush();
			tActualizado = true;
		}
		return tActualizado;
	}

	private Torneo modificarTorneo(Torneo torneoBD, Torneo torneoNuevo) {
		torneoBD.setId_torneo(torneoNuevo.getId_torneo());
		torneoBD.setNombre(torneoNuevo.getNombre());
		torneoBD.setDescripcion(torneoNuevo.getDescripcion());
		torneoBD.setFecha_inicio(torneoNuevo.getFecha_inicio());
		torneoBD.setFecha_fin(torneoNuevo.getFecha_fin());
		torneoBD.setImagen(torneoNuevo.getImagen());
		torneoBD.setPremiacion(torneoNuevo.getPremiacion());
		torneoBD.setDesc_premiacion(torneoNuevo.getDesc_premiacion());
		torneoBD.setGenero(torneoNuevo.getGenero());
		torneoBD.setModalidad(torneoNuevo.getModalidad());

		return torneoBD;
	}

	@Override
	public boolean eliminarTorneo(Torneo torneo) {
		// TODO Auto-generated method stub
		return false;
	}

}
