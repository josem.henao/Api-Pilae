package co.com.jose.pilae.dominio.usuario;

import java.util.List;

import co.com.jose.pilae.model.Torneo;


public interface ITorneoDominio {
	boolean insertarTorneo(Torneo torneo);

	Torneo consultarTorneoPorId(int id_torneo);

	List<Torneo> consultarTorneoPorNombre(String nombre);

	boolean actualizarTorneo(Torneo torneo);

	boolean eliminarTorneo(Torneo torneo);

}
